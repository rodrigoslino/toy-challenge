import * as ActionTypes from "../constants/actionTypes";
import reducer from "./blocks";
import initialState from "./initialState";

describe("Reducers::Blocks", () => {
  const getInitialState = () => {
    return initialState().blocks;
  };

  it("should set initial state by default", () => {
    const action = { type: "unknown" };
    const expected = getInitialState();

    expect(reducer(undefined, action)).toEqual(expected);
  });

  it("should handle GET_BLOCKS_START", () => {
    const appState = {
      list: [],
    };
    const action = { type: ActionTypes.GET_BLOCKS_START };
    const expected = {
      list: [],
      loading: true,
      error: "",
    };

    expect(reducer(appState, action)).toEqual(expected);
  });

  it("should handle GET_BLOCKS_SUCCESS", () => {
    const appState = {
      list: [],
    };
    const action = {
      type: ActionTypes.GET_BLOCKS_SUCCESS,
      res: {
        data: [
          {
            id: 5,
            type: "blocks",
            attributes: {
              data: "The Human Torch",
            },
          },
        ],
      },
    };
    const expected = {
      list: action.res.data,
      loading: false,
      error: "",
    };

    expect(reducer(appState, action)).toEqual(expected);
  });

  it("should handle GET_BLOCKS_FAILURE", () => {
    const appState = {
      list: [],
      loading: false,
      error: "",
    };
    const action = { type: ActionTypes.GET_BLOCKS_FAILURE };
    const expected = {
      list: [],
      loading: false,
      error: "Error on getting blocks",
    };

    expect(reducer(appState, action)).toEqual(expected);
  });
});
