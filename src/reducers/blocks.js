import {
  GET_BLOCKS_START,
  GET_BLOCKS_SUCCESS,
  GET_BLOCKS_FAILURE,
} from "../constants/actionTypes";
import initialState from "./initialState";

export default function blocksReducer(state = initialState().blocks, action) {
  switch (action.type) {
    case GET_BLOCKS_START:
      return {
        ...state,
        list: [],
        loading: true,
        error: "",
      };
    case GET_BLOCKS_SUCCESS:
      return {
        ...state,
        list: action.res.data,
        loading: false,
        error: "",
      };
    case GET_BLOCKS_FAILURE:
      return {
        ...state,
        list: [],
        loading: false,
        error: "Error on getting blocks",
      };
    default:
      return state;
  }
}
