import * as ActionTypes from "../constants/actionTypes";
import * as ActionCreators from "./blocks";
import mockFetch from "cross-fetch";

jest.mock("cross-fetch");

describe("Actions Blocks", () => {
  const dispatch = jest.fn();

  afterAll(() => {
    dispatch.mockClear();
    mockFetch.mockClear();
  });

  const node = {
    url: "http://localhost:3002",
    online: false,
    name: null,
  };

  it("should fetch the blocks", async () => {
    mockFetch.mockReturnValueOnce(
      Promise.resolve({
        status: 200,
        json() {
          return Promise.resolve({
            data: [
              {
                id: 5,
                type: "blocks",
                attributes: {
                  data: "The Human Torch",
                },
              },
            ],
          });
        },
      })
    );
    await ActionCreators.getBlock(node)(dispatch);
    const expected = [
      {
        type: ActionTypes.GET_BLOCKS_START,
        node,
      },
      {
        type: ActionTypes.GET_BLOCKS_SUCCESS,
        node,
        res: {
          data: [
            {
              id: 5,
              type: "blocks",
              attributes: {
                data: "The Human Torch",
              },
            },
          ],
        },
      },
    ];

    expect(dispatch.mock.calls.flat()).toEqual(expected);
  });

  it("should fail to getch the blocks", async () => {
    mockFetch.mockReturnValueOnce(
      Promise.resolve({
        status: 400,
      })
    );
    await ActionCreators.getBlock(node)(dispatch);
    const expected = [
      {
        type: ActionTypes.GET_BLOCKS_START,
        node,
      },
      {
        type: ActionTypes.GET_BLOCKS_FAILURE,
        node,
      },
    ];

    expect(dispatch.mock.calls.flat()).toEqual(expected);
  });
});
