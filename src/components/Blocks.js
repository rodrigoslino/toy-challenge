import { makeStyles } from "@material-ui/core";
import React from "react";

function Blocks({ blocks }) {
  const { loading, error, list } = blocks;
  const classes = useStyles();
  return (
    <>
      {loading && "Loading..."}
      {error}
      {list?.map((block) => (
        <div
          key={block.id + block?.attributes?.data}
          className={classes.blockSummary}
        >
          <span className={classes.blockTitle}>
            {block.id.padStart(3, "0")}
          </span>
          <span className={classes.blockDescription}>
            {block.attributes.data}
          </span>
        </div>
      ))}
    </>
  );
}

const useStyles = makeStyles((theme) => ({
  blockSummary: {
    padding: 8,
    background: "rgba(0, 0, 0, 0.12)",
    marginBottom: "4px",
    borderRadius: "2px",
  },
  blockTitle: { color: "#304FFE", fontSize: "10px", fontWeight: "bold" },
  blockDescription: {
    display: "block",
  },
}));

export default Blocks;
